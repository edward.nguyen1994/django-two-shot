from django.shortcuts import render, redirect

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib.auth.models import User


# def signup(request):
#     if request.method == "POST":
#         # process data from form
#         form = UserCreationForm(request.POST)
#         if form.is_valid():

#             # solution 1

#             # username = request.POST["username"]
#             # password = request.POST["password1"]
#             # user = User.objects.create_user(
#             #     username=username, password=password
#             # )
#             # login(request, user)

#             # solution 2

#             # user = form.save()
#             # login(request, user)

#             return redirect("home")
#             # need return for both solutions
#     else:
#         form = UserCreationForm()
#     return render(request, "registration/signup.html", {"form": form})


def signup_accounts(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)
