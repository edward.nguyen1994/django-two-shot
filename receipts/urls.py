from django.urls import path

from receipts.views import *

urlpatterns = [
    path("", list_receipts, name="home"),
    path("create/", create_receipts, name="create_receipt"),
    path("categories/", list_categories, name="categories_list"),
    path("accounts/", list_accounts, name="accounts_list"),
    path("categories/create/", create_categories, name="create_category"),
    path("accounts/create/", create_accounts, name="create_account"),
]
