from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from receipts.models import *
from receipts.forms import *

# Create your views here.


@login_required
def list_receipts(request):
    context = {
        "receipts_list": Receipt.objects.filter(purchaser=request.user),
    }
    return render(request, "receipts/list.html", context)
    # keys in context dictionary = tags in template html files


@login_required
def create_receipts(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            # useful if need to set author = self.user
            #   useful if need to edit form before saving it
            # like below for purchaser
            receipt.purchaser = request.user
            # not using self.request.user bc not referring to instance, just calling a fxn
            # in other examples, self should be passed in somewhere
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(
        request,
        "receipts/create.html",
        context,
    )


@login_required
def list_categories(request):
    context = {
        "categories_list": ExpenseCategory.objects.filter(owner=request.user),
    }
    return render(request, "receipts/categories/list.html", context)


@login_required
def list_accounts(request):
    context = {
        "accounts_list": Account.objects.filter(owner=request.user),
    }
    return render(request, "receipts/accounts/list.html", context)


@login_required
def create_categories(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("categories_list")
    else:
        form = CategoryForm()
    context = {"form": form}
    return render(
        request,
        "receipts/categories/create.html",
        context,
    )


@login_required
def create_accounts(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("accounts_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(
        request,
        "receipts/accounts/create.html",
        context,
    )
